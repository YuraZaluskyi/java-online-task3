package com.yurazaluskyi.movieTheater;

import com.yurazaluskyi.movieTheater.cinema.MovieTheater;
import com.yurazaluskyi.movieTheater.managment.MovieManager;
import com.yurazaluskyi.movieTheater.managment.MovieUser;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        MovieTheater movieTheatre = new MovieTheater();
        Scanner sc = new Scanner(System.in);
        MovieManager movieManager = new MovieManager(sc, movieTheatre);
        MovieUser movieUser = new MovieUser(sc, movieTheatre);

        while (true) {
            printMenu();
            System.out.println();
            int choice = sc.nextInt();
            switch (choice) {
                case 1:
                    while (true) {
                        printUserMenu();
                        System.out.println();
                        int choiceUser = sc.nextInt();
                        switch (choiceUser) {
                            case 1:
                                movieUser.printListMovies();

                                break;
                            case 2:
                                movieUser.buyTicket();
                                break;
                            case 3:
                                movieUser.bookTicket();
                                break;
                            default:
                                System.out.println("goodbye User");
                                return;
                        }

                    }


                case 2:
                    while (true) {
                        printManagerMenu();
                        System.out.println();
                        int choiceManager = sc.nextInt();
                        switch (choiceManager) {
                            case 1:
                                movieManager.printListMovies();

                                break;
                            case 2:
                                movieManager.addMovie();

                                break;
                            case 3:
                                movieManager.delMovie();

                                break;
                            case 4:
                                movieManager.addGenre();

                                break;
                            case 5:
                                movieManager.delGenre();

                                break;
                            case 6:
                                movieManager.addSession();

                                break;
                            case 7:
                                movieManager.delSession();

                                break;
                            case 8:
                                movieManager.addHall();

                                break;
                            case 9:
                                movieManager.delHall();

                                break;
                            case 10:
                                movieManager.setPriceZoneHall();

                                break;
                            case 11:
                                movieManager.setStatusPlaces();

                                break;
                            case 12:
                                movieManager.printHall();

                                break;

                            default:
                                System.out.println("goodbye Manager");
                                return;
                        }
                    }

                default:
                    System.out.println("goodbye");
                    return;
            }
        }

    }

    static void printManagerMenu() {
        System.out.println("1 - print the List of the Movies");
        System.out.println("2 - add Movie");
        System.out.println("3 - delete Movie");
        System.out.println("4 - add Genre");
        System.out.println("5 - delete Genre");
        System.out.println("6 - add the Session");
        System.out.println("7 - delete the Session");
        System.out.println("8 - add Hall");
        System.out.println("9 - delete the Hall");
        System.out.println("10 - set Prices for Zone of the Hall");
        System.out.println("11 - set Status for all places as free");
        System.out.println("12 - print Hall");
        System.out.println("e x i t - 100");
    }

    static void printUserMenu() {
        System.out.println("1 - print list movies");
        System.out.println("2 - to buy the Ticket");
        System.out.println("3 - to book the Ticket");
        System.out.println("e x i t - 100");
    }

    static void printMenu() {
        System.out.println("1 - UserMenu");
        System.out.println("2 - ManagerMenu");
    }
}
