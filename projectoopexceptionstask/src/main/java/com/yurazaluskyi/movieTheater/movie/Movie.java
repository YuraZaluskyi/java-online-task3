package com.yurazaluskyi.movieTheater.movie;

import com.yurazaluskyi.movieTheater.genre.Genre;
import com.yurazaluskyi.movieTheater.session.Session;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Movie {
    private String title;
    private int length;
    private int price;
    private List<Session> sessions;
    private List<Genre> genres;

    public Movie() {
        sessions = new ArrayList<Session>();
        genres = new ArrayList<Genre>();
    }

    public Movie(String title, int length, int price) {
        sessions = new ArrayList<Session>();
        genres = new ArrayList<Genre>();
        this.title = title;
        this.length = length;
        this.price = price;
    }

    public Movie(String title) {
        sessions = new ArrayList<Session>();
        genres = new ArrayList<Genre>();
        this.title = title;
    }

    public Movie(String title, int length) {
        this.title = title;
        this.length = length;
        this.price = price;
        sessions = new ArrayList<Session>();
        genres = new ArrayList<Genre>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public void setSessions(List<Session> sessions) {
        this.sessions = sessions;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public void addSession(Session session){
        sessions.add(session);
    }

    public void delSession(int startSession){
        Iterator<Session> s = sessions.iterator();
        while (s.hasNext()){
            Session o = s.next();
            if (o.getStartSession() == startSession){
                s.remove();
            }
        }
    }

    public void addGenre(Genre genre){
        genres.add(genre);
    }

    public void delGenre(String title){
        Iterator<Genre> g = genres.iterator();
        while (g.hasNext()){
            Genre o = g.next();
            if (o.getTitleGenre().equalsIgnoreCase(title)){
                g.remove();
            }
        }
    }

    public void printListGenre(){
        for (Genre j : getGenres()){
            System.out.println(j.getTitleGenre());
        }
    }

    public Session getSession(int tm){
        Iterator<Session> i = sessions.iterator();
        while (i.hasNext()){
            Session o = i.next();
            if (o.getStartSession() == tm){
                return o;
            }
        } return null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Movie movie = (Movie) o;

        if (length != movie.length) return false;
        if (price != movie.price) return false;
        return title != null ? title.equals(movie.title) : movie.title == null;
    }

    @Override
    public int hashCode() {
        int result = title != null ? title.hashCode() : 0;
        result = 31 * result + length;
        result = 31 * result + price;
        return result;
    }

    @Override
    public String toString() {
        return "movie{" +
                "title='" + title + '\'' +
                ", length=" + length +
                ", price=" + price +
                ", sessions=" + sessions +
                ", genres=" + genres +
                '}';
    }



}
