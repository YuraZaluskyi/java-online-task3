package com.yurazaluskyi.movieTheater.managment;

import com.yurazaluskyi.movieTheater.cinema.MovieTheater;
import com.yurazaluskyi.movieTheater.hall.Hall;

import java.util.Scanner;

public class MovieUser {
    private Scanner sc;
    private MovieTheater movieTheater;

    public MovieUser(Scanner sc, MovieTheater movieTheater) {
        this.sc = sc;
        this.movieTheater = movieTheater;
    }

    public void printListMovies() {
        movieTheater.printListMoviesUser();
    }

    public void buyTicket() {
        movieTheater.printListMovies();
        System.out.println("Enter the title of the Movie would you like to see: ");
        String titleMovie = sc.next();
        System.out.println("Choose the session: ");
        int ssn = sc.nextInt();
        System.out.println("                s c r e e n");
        System.out.println();
        String w = movieTheater.getMovie(titleMovie).getSession(ssn).getHall().getTitleHall();
        Hall hll = movieTheater.getHall(w);
        hll.printPlacesHall();
        System.out.println();
        System.out.println();
        System.out.println("                s c r e e n");
        System.out.println();
        movieTheater.getMovie(titleMovie).getSession(ssn).printPlaces();
        System.out.println("           Choose the place: ");
        System.out.println();
        System.out.println("Choose the row: ");
        int rw = sc.nextInt();
        System.out.println("Choose the column");
        int clmn = sc.nextInt();
        int priceTicket = movieTheater.getMovie(titleMovie).getPrice() +
                movieTheater.getMovie(titleMovie).getSession(ssn).getPrice() + hll.getPlaces()[rw][clmn];
        movieTheater.getMovie(titleMovie).getSession(ssn).setStatusPlace("A", rw, clmn);
        System.out.println();
        System.out.print("Ticket price: " + priceTicket);

        System.out.println();
        System.out.println();
    }

    public void bookTicket() {
        buyTicket();
    }

}
