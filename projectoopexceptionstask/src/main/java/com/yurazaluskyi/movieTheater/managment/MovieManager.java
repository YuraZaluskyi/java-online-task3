package com.yurazaluskyi.movieTheater.managment;

import com.yurazaluskyi.movieTheater.cinema.MovieTheater;
import com.yurazaluskyi.movieTheater.genre.Genre;
import com.yurazaluskyi.movieTheater.hall.Hall;
import com.yurazaluskyi.movieTheater.movie.Movie;
import com.yurazaluskyi.movieTheater.session.Session;

import java.util.List;
import java.util.Scanner;

public class MovieManager {
    private Scanner sc;
    private MovieTheater movieTheater;


    public MovieManager(Scanner sc, MovieTheater movieTheater) {
        this.sc = sc;
        this.movieTheater = movieTheater;
    }

    public void addHall() {
        System.out.println("Enter the Title of the Hall: ");
        String title = sc.next();
        System.out.println("Enter quantity of the Rows: ");
        int row = sc.nextInt();
        System.out.println("Enter quantity of the Columns: ");
        int column = sc.nextInt();
        Hall hall = new Hall(title, row, column);
        movieTheater.addHall(hall);
    }

    public void delHall() {
        System.out.println("Enter the Title of the Hall would you like delete: ");
        String title = sc.next();
        movieTheater.delHall(title);
    }

    public void addSession() {
        System.out.println("Enter the title of movie for which would you like add the Session: ");
        String titleMovie = sc.next();
        System.out.println("Enter start of the Session: ");
        int start = sc.nextInt();
        System.out.println("Enter the price for the ticket: ");
        int price = sc.nextInt();
        System.out.println("Enter the title of the Hall for view of the movie: ");
        String titleHall = sc.next();
        Hall hall = movieTheater.getHall(titleHall);
        Hall hallSession = new Hall(titleHall, hall.getRow(), hall.getColumn());
        hallSession.statusPlaces();
        movieTheater.getMovie(titleMovie).addSession(new Session(start, price, hallSession));

    }

    public void delSession() {
        System.out.println("Enter the title of movie for which would you like delete the Session: ");
        String titleMovie = sc.next();
        System.out.println("Enter the start Session would you like delete: ");
        int start = sc.nextInt();
        movieTheater.getMovie(titleMovie).delSession(start);

    }

    public void addGenre() {
        System.out.println("Enter the title of movie for which would you like add the Genre: ");
        String titleMovie = sc.next();
        System.out.println("Enter the title of the Genre: ");
        String titleGenre = sc.next();
        movieTheater.getMovie(titleMovie).addGenre(new Genre(titleGenre));

    }

    public void delGenre() {
        System.out.println("Enter the title of Movie for which would you like del the Genre: ");
        String titleMovie = sc.next();
        System.out.println("Enter the title of the Genre would you like delete: ");
        String titleGenre = sc.next();
        movieTheater.getMovie(titleMovie).delGenre(titleGenre);

    }

    public void addMovie() {
        System.out.println("Enter the Title of the movie: ");
        String title = sc.next();
        System.out.println("Enter the Length of the movie: ");
        int length = sc.nextInt();
        System.out.println("Enter the Price for the ticket: ");
        int price = sc.nextInt();
        Movie movie = new Movie(title, length, price);
        movieTheater.addMovie(movie);
    }

    public void delMovie() {
        System.out.println("Enter the Title the Movie would you like delete");
        String title = sc.next();
        movieTheater.delMovie(title);
    }

    public void printHall() {
        List<Hall> h = movieTheater.getHalls();
        for (Hall i : h) {
            System.out.println(i);
            System.out.println();
        }
        System.out.println();
    }

    public void setPriceZoneHall() {
        System.out.println("Enter the title of the Hall: ");
        String titleH = sc.next();
        System.out.println("Enter the last Row of the First Zone");
        int row1 = sc.nextInt();
        System.out.println("Enter the Price of the First Zone");
        int prc1 = sc.nextInt();
        System.out.println("Enter the last Row of the Second Zone");
        int row2 = sc.nextInt();
        System.out.println("Enter the Price of the Second Zone");
        int prc2 = sc.nextInt();
        System.out.println("Enter the Price of the Third Zone;");
        int prc3 = sc.nextInt();
        movieTheater.getHall(titleH).pricePlace(row1, row2, prc1, prc2, prc3);
        movieTheater.getHall(titleH).printPlacesHall();
    }

    public void printListMovies() {
        movieTheater.printListMovies();
    }

    public void setStatusPlaces() {
        System.out.println("Enter the title of the Hall");
        String title = sc.next();
        movieTheater.getHall(title).statusPlaces();
    }

    public void printPlHall() {
        System.out.println("title hall");
        String th = sc.next();
        movieTheater.getHall(th).printFreePlaces();

    }
}
