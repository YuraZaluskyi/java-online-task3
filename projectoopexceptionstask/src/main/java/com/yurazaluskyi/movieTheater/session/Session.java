package com.yurazaluskyi.movieTheater.session;

import com.yurazaluskyi.movieTheater.hall.Hall;


public class Session {
    private int startSession;
    private int price;
    private Hall hall;

    public Session() {
    }

    public Session(int startSession, int price, Hall hall) {
        this.startSession = startSession;
        this.price = price;
        this.hall = hall;
    }

    public Session(int startSession) {
        this.startSession = startSession;
        hall = new Hall();
    }

    public int getStartSession() {
        return startSession;
    }

    public void setStartSession(int startSession) {
        this.startSession = startSession;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public void setStatusPlace(String sgn, int rw, int cl) {
        getHall().getBookPlaces()[rw][cl] = sgn;
    }


    public void printPlaces() {
        for (int i = 0; i < getHall().getRow(); i++) {
            for (int j = 0; j < getHall().getColumn(); j++) {
                System.out.print(" [ " + getHall().getBookPlaces()[i][j] + " ]");
            }
            System.out.println();
        }
    }

    @Override
    public String toString() {
        return "Session{" +
                "startSession=" + startSession +
                '}';
    }
}
