package com.yurazaluskyi.movieTheater.genre;

import java.util.Objects;

public class Genre {
    private String titleGenre;

    public Genre() {
    }

    public Genre(String titleGenre) {
        this.titleGenre = titleGenre;
    }

    public String getTitleGenre() {
        return titleGenre;
    }

    public void setTitleGenre(String titleGenre) {
        this.titleGenre = titleGenre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Genre genre = (Genre) o;

        return Objects.equals(titleGenre, genre.titleGenre);
    }

    @Override
    public int hashCode() {
        return titleGenre != null ? titleGenre.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "titleGenre='" + titleGenre + '\'' +
                '}';
    }
}
