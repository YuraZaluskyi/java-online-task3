package com.yurazaluskyi.movieTheater.hall;

import java.util.Arrays;
import java.util.Objects;

public class Hall {
    private String titleHall;
    private int row;
    private int column;
    private String bookPlaces[][] = new String[row][column];
    private int places[][] = new int[row][column];

    public Hall() {
    }

    public Hall(String titleHall) {
        this.titleHall = titleHall;
    }

    public Hall(String titleHall, int row, int column) {
        this.titleHall = titleHall;
        this.row = row;
        this.column = column;
        places = new int[row][column];
        bookPlaces = new String[row][column];
    }


    public String getTitleHall() {
        return titleHall;
    }

    public void setTitleHall(String titleHall) {
        this.titleHall = titleHall;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public String[][] getBookPlaces() {
        return bookPlaces;
    }

    public void setBookPlaces(String[][] bookPlaces) {
        this.bookPlaces = bookPlaces;
    }

    public int[][] getPlaces() {
        return places;
    }

    public void setPlaces(int[][] places) {
        this.places = places;
    }

    public void printPlacesHall() {
        for (int i = 0; i < getRow(); i++) {
            for (int j = 0; j < getColumn(); j++) {
                System.out.print(" [ " + places[i][j] + " ]");
            }
            System.out.println();
        }
    }

    public void printFreePlaces() {
        printPlacesHall();
    }

    public void statusPlaces() {
        for (int i = 0; i < getRow(); i++) {
            for (int j = 0; j < getColumn(); j++) {
                bookPlaces[i][j] = "F";
            }
        }
    }

    public void pricePlace(int firstZone, int secondZone, int prcFrstZn, int prcScndZn, int prcThrdZn) {
        for (int i = 0; i < firstZone; i++) {
            for (int j = 0; j < getColumn(); j++) {
                places[i][j] = prcFrstZn;
            }
        }
        for (int i = firstZone; i < secondZone; i++) {
            for (int j = 0; j < getColumn(); j++) {
                places[i][j] = prcScndZn;
            }
        }
        for (int i = secondZone; i < getRow(); i++) {
            for (int j = 0; j < getColumn(); j++) {
                places[i][j] = prcThrdZn;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hall hall = (Hall) o;

        if (row != hall.row) return false;
        if (column != hall.column) return false;
        if (!Objects.equals(titleHall, hall.titleHall)) return false;
        if (!Arrays.deepEquals(bookPlaces, hall.bookPlaces)) return false;
        return Arrays.deepEquals(places, hall.places);
    }

    @Override
    public int hashCode() {
        int result = titleHall != null ? titleHall.hashCode() : 0;
        result = 31 * result + row;
        result = 31 * result + column;
        result = 31 * result + Arrays.deepHashCode(bookPlaces);
        result = 31 * result + Arrays.deepHashCode(places);
        return result;
    }

    @Override
    public String toString() {
        return "hall{" +
                "titleHall='" + titleHall + '\'' +
                ", row=" + row +
                ", column=" + column +
                '}';
    }
}
