package com.yurazaluskyi.movieTheater.cinema;

import com.yurazaluskyi.movieTheater.hall.Hall;
import com.yurazaluskyi.movieTheater.movie.Movie;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MovieTheater {
    private List<Hall> halls = new ArrayList<>();
    private List<Movie> movies = new ArrayList<>();

    public MovieTheater() {
    }

    public List<Hall> getHalls() {
        return halls;
    }

    public void setHalls(List<Hall> halls) {
        this.halls = halls;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public void addHall(Hall hall) {
        halls.add(hall);
    }

    public void delHall(String titleHall) {
        Iterator<Hall> h = halls.iterator();
        while (h.hasNext()) {
            Hall o = h.next();
            if (o.getTitleHall().equalsIgnoreCase(titleHall)) {
                h.remove();
            }
        }
    }

    public void addMovie(Movie movie) {
        movies.add(movie);
    }

    public void delMovie(String titleMovie) {
        Iterator<Movie> m = movies.iterator();
        while (m.hasNext()) {
            Movie o = m.next();
            if (o.getTitle().equalsIgnoreCase(titleMovie)) {
                m.remove();
            }
        }

    }

    public Hall getHall(String nameHall) {
        Iterator<Hall> i = halls.iterator();
        while (i.hasNext()) {
            Hall o = i.next();
            if (o.getTitleHall().equalsIgnoreCase(nameHall)) {
                return o;
            }
        }
        return null;
    }

    public Movie getMovie(String nameMovie) {
        Iterator<Movie> i = movies.iterator();
        while (i.hasNext()) {
            Movie o = i.next();
            if (o.getTitle().equalsIgnoreCase(nameMovie)) {
                return o;
            }
        }
        return null;
    }

    public void printListMovies() {
        for (Movie i : getMovies()) {
            System.out.println(i.getTitle() + " " + i.getLength() + " " + i.getPrice() +
                    "  " + i.getGenres() + "  " + i.getSessions());
            System.out.println();
        }
        System.out.println();
    }

    public void printListMoviesUser() {
        for (Movie i : getMovies()) {
            System.out.println(i.getTitle() + "  " + i.getGenres() + "  " + i.getSessions());
            System.out.println();
        }
        System.out.println();
    }

    @Override
    public String toString() {
        return "MovieTheater{" +
                "halls=" + halls +
                ", movies=" + movies +
                '}';
    }
}
